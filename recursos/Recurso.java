/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.aula.abrigopet.recursos;

/**
 *
 * @author suporte
 */
public class Recurso {
    private String descricao;
    private double quantidade;
    private ERecursoTipo tipoDeRecurso;

    public Recurso() {
    }

    public Recurso(String descricao, double quantidade, ERecursoTipo tipoDeRecurso) {
        this.descricao = descricao;
        this.quantidade = quantidade;
        this.tipoDeRecurso = tipoDeRecurso;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(double quantidade) {
        this.quantidade = quantidade;
    }

    public ERecursoTipo getTipoDeRecurso() {
        return tipoDeRecurso;
    }

    public void setTipoDeRecurso(ERecursoTipo tipoDeRecurso) {
        this.tipoDeRecurso = tipoDeRecurso;
    }
    
}
