/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.aula.abrigopet.animais;

import br.edu.vianna.aula.abrigopet.racas.ECatRaca;
import java.time.LocalDate;

/**
 *
 * @author suporte
 */
public class Gato  extends Animal{
    private ECatRaca raca;

    public Gato() {
    }

    public Gato(ECatRaca raca) {
        this.raca = raca;
    }

    public Gato(ECatRaca raca, String nome, LocalDate dataNasc, boolean ehVacinado, boolean ehCastrado) {
        super(nome, dataNasc, ehVacinado, ehCastrado);
        this.raca = raca;
    }

    public ECatRaca getRaca() {
        return raca;
    }

    public void setRaca(ECatRaca raca) {
        this.raca = raca;
    }
    
    
    
    
    
    
}
