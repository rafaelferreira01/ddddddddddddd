/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.aula.abrigopet.animais;

import br.edu.vianna.aula.abrigopet.racas.EDogRaca;
import java.time.LocalDate;

/**
 *
 * @author suporte
 */
public class Cachorro extends Animal{
    private EDogRaca raca;

    public Cachorro() {
    }

    public Cachorro(EDogRaca raca) {
        this.raca = raca;
    }

    public Cachorro(EDogRaca raca, String nome, LocalDate dataNasc, boolean ehVacinado, boolean ehCastrado) {
        super(nome, dataNasc, ehVacinado, ehCastrado);
        this.raca = raca;
    }

    public EDogRaca getRaca() {
        return raca;
    }

    public void setRaca(EDogRaca raca) {
        this.raca = raca;
    }
    

    
    
    
    
}
