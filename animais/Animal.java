/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.aula.abrigopet.animais;

import java.time.LocalDate;

/**
 *
 * @author suporte
 */
public abstract class Animal {
    
    private String nome;
    private LocalDate dataNasc;
    private boolean ehVacinado;
    private boolean ehCastrado;

    public Animal() {
    }

    public Animal(String nome, LocalDate dataNasc, boolean ehVacinado, boolean ehCastrado) {
        this.nome = nome;
        this.dataNasc = dataNasc;
        this.ehVacinado = ehVacinado;
        this.ehCastrado = ehCastrado;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public LocalDate getDataNasc() {
        return dataNasc;
    }

    public void setDataNasc(LocalDate dataNasc) {
        this.dataNasc = dataNasc;
    }

    public boolean isEhVacinado() {
        return ehVacinado;
    }

    public void setEhVacinado(boolean ehVacinado) {
        this.ehVacinado = ehVacinado;
    }

    public boolean isEhCastrado() {
        return ehCastrado;
    }

    public void setEhCastrado(boolean ehCastrado) {
        this.ehCastrado = ehCastrado;
    }
    
    
    
}
