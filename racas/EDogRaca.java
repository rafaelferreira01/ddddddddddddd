/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.aula.abrigopet.racas;

/**
 *
 * @author suporte
 */
public enum EDogRaca {
    BULLDOG_FRANCES, PIT_BULL, SPITZ_ALEMAO, PASTOR_ALEMAO, BASSET, SCHNAUZER,
    POODLE, ROTTWEILER, LABRADOR, PINSCHER, LHASA_APSO, GOLDEN_RETRIEVER, YORKSHIRE,
    BODER_COLLIE, BEAGLE, VIRA_LATA, PUG, MALTES, SHIH_TZU, BULDOGUE, CANI_CORSO;
}
