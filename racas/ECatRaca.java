/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.aula.abrigopet.racas;

/**
 *
 * @author suporte
 */
public enum ECatRaca {
    PERSA, HIMALAIA, SIAMES, MAINE_COON, ANGORA, SPHYNX, RAGDOLL, ASHERA, AMERICAN_SHORTHAIR, VIRA_LATA, SIBERIANO, BURMESE;
    
}
