/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.aula.abrigopet.abrigo;

import br.edu.vianna.aula.abrigopet.recursos.Recurso;
import br.edu.vianna.aula.abrigopet.animais.Animal;
import br.edu.vianna.aula.abrigopet.pessoas.Adotante;
import java.util.ArrayList;

/**
 *
 * @author suporte
 */
public class Abrigo {
    
    public ArrayList<Animal> listaPetsDisponiveis;
    public ArrayList<Adotante> listaAdotantes;
    public ArrayList<Recurso> listaRecursos;

    public Abrigo(ArrayList<Animal> listaPetsDisponiveis, ArrayList<Adotante> listaAdotantes, ArrayList<Recurso> listaRecursos) {
        this.listaPetsDisponiveis = listaPetsDisponiveis;
        this.listaAdotantes = listaAdotantes;
        this.listaRecursos = listaRecursos;
    }

    public ArrayList<Animal> getListaPetsDisponiveis() {
        return listaPetsDisponiveis;
    }

    public void addPetNovo(Animal petNovo) {
        this.listaPetsDisponiveis.add(petNovo);
    }
    
    public void removePet(int indiciePet) {
        this.listaPetsDisponiveis.remove(indiciePet);
    }

    public ArrayList<Adotante> getListaAdotantes() {
        return listaAdotantes;
    }

    public void addAdotanteNovo(Adotante adotanteNovo) {
        this.listaAdotantes.add(adotanteNovo);
    }

    public void removeAdotante(int indicieAdotantes) {
        this.listaAdotantes.remove(indicieAdotantes);
    }
    
    public ArrayList<Recurso> getListaRecursos() {
        return listaRecursos;
    }

    public void addRecursoNovo(Recurso recursoNovo) {
        this.listaRecursos.add(recursoNovo);
    }
    
    public void removeRecurso(int indicieRecursos) {
        this.listaRecursos.remove(indicieRecursos);
    }


    

   
    
    
    
    
    
    
    
    
    
    
}
