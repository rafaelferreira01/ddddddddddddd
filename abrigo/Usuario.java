/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.aula.abrigopet.abrigo;

/**
 *
 * @author suporte
 */
public class Usuario {
    
    private String userName;
    private int senha;

    public Usuario() {
    }

    public Usuario(String userName, int senha) {
        this.userName = userName;
        this.senha = senha;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getSenha() {
        return senha;
    }

    public void setSenha(int senha) {
        this.senha = senha;
    }
    
    
}
