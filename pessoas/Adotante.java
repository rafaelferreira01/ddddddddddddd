/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.aula.abrigopet.pessoas;

import br.edu.vianna.aula.abrigopet.animais.Animal;
import java.util.ArrayList;


/**
 *
 * @author suporte
 */
public class Adotante {
    private String nome;
    private int cpf;
    private int telefone;
    private ArrayList<Animal> meusPets;

    public Adotante() {
    }

    public Adotante(String nome, int cpf, int telefone) {
        this.nome = nome;
        this.cpf = cpf;
        this.telefone = telefone;
        meusPets = new ArrayList<>();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getCpf() {
        return cpf;
    }

    public void setCpf(int cpf) {
        this.cpf = cpf;
    }

    public int getTelefone() {
        return telefone;
    }

    public void setTelefone(int telefone) {
        this.telefone = telefone;
    }

    public ArrayList<Animal> getMeusPets() {
        return meusPets;
    }

    public void addPet(Animal pet) {
        this.meusPets.add(pet);
    }
    
    public void removePet(int indiciePet) {
        this.meusPets.remove(indiciePet);
    }

    
    
    
    
}
